#ifndef IOTSTAT_H_
#define IOTSTAT_H_

#include "DHT.h"
#include "FreeRTOS.h"

#define MAX_POLL_ATTEMPTS_BEFORE_TIMEOUT 3
#define RETRY_POLLING_PERIOD_SEC 2.1

void initTempSensor( int sensorPin );
bool pollTempSensor( float* currentTemperature, float* currentHumidity );

#endif // IOTSTAT_H_