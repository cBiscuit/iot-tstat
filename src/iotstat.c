#include "iotstat.h"

void initTempSensor( int sensorPin )
{
    setDHTgpio(sensorPin);
    configPRINTF( ( "DHT Sensor Initialized on pin %i\r\n", sensorPin ) );
}

/*-----------------------------------------------------------*/

bool pollTempSensor( float* currentTemperature, float* currentHumidity )
{
    int ret = -1;

    for (int inx=0; inx < MAX_POLL_ATTEMPTS_BEFORE_TIMEOUT; inx++)
    {
        ret = readDHT();
        if (ret == DHT_OK)
        {
            *currentTemperature = getTemperature();
            *currentHumidity = getHumidity();
			return true;
        }
        else
        {
			configPRINTF( ( "Failed call %i: ", inx+1 ) );
            errorHandler(ret);
        }
        ets_delay_us(RETRY_POLLING_PERIOD_SEC * 1000000);
    }
	return false;
}