import greengrasssdk
import json

AGGREGATE_BUCKET_SIZE = 10
AGGREGATE_TOPIC = 'spoke/thermostats'

# Creating a greengrass core sdk client
client = greengrasssdk.client('iot-data')

class tStatRemote:
    name = ''
    _temperature = []
    _humidity = []
    _messageTimes = []
    _sensorTics = []
    messageCounter = 0
    
    def __init__(self, name):
        self.name = name
        
    def addMeasurement(self, temperature, humidity, time, tics):
        while len(self._temperature) >= AGGREGATE_BUCKET_SIZE:
            self._temperature.pop(0)
            self._humidity.pop(0)
            self._messageTimes.pop(0)
            self._sensorTics.pop(0)
        
        self._temperature.append(temperature)
        self._humidity.append(humidity)
        self._messageTimes.append(time)
        self._sensorTics.append(tics)
        
    def getAverageTemperature(self):
        return sum(self._temperature) / len(self._temperature)
        
    def getAverageHumidity(self):
        return sum(self._humidity) / len(self._humidity)
        
    def getLatestMeasurementTime(self):
        return self._messageTimes[-1]
        
    def getLastSensorPoll(self):
        return self._sensorTics[-1]

thermostats = {}

# function_handler: the main entry point for interacting with this lambda function
def function_handler(event, context):
    global thermostats
    
    tStatKey = ''
    
    if 'spoke__externalKey' in event.keys() and event['spoke__mode'] == 'device':
        tStatKey = event['spoke__externalKey']
        
        # add thermostat to tracking dictionary if required
        if tStatKey not in thermostats:
            thermostats[tStatKey] = tStatRemote(tStatKey)
            
        thermostats[tStatKey].addMeasurement(event['temperature'], event['humidity'], 0, event['lastSensorPoll'])
        thermostats[tStatKey].messageCounter = thermostats[tStatKey].messageCounter + 1
        
        if (thermostats[tStatKey].messageCounter >= AGGREGATE_BUCKET_SIZE):
            thermostats[tStatKey].messageCounter = 0
            client.publish(
                topic = '{0}/update'.format(AGGREGATE_TOPIC),
                payload = json.dumps({
                    'spoke__externalKey': thermostats[tStatKey].name,
                    'spoke__mode': 'device',
                    'spoke__messageTimestamp': thermostats[tStatKey].getLatestMeasurementTime(),
                    'spoke__thingType': 'Environment',
                    'spoke__gatewayId': 'Environment',
                    'temperature': thermostats[tStatKey].getAverageTemperature(),
                    'humidity': thermostats[tStatKey].getAverageHumidity(),
                    'lastSensorPoll': thermostats[tStatKey].getLastSensorPoll(),
                })
            )
    else:
        client.publish(
            topic = '{0}/error'.format(AGGREGATE_TOPIC),
            payload = json.dumps({'Message': 'ERROR: Invalid message.'})
        )
        
    return