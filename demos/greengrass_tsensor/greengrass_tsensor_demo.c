/*
 * Amazon FreeRTOS V201906.00 Major
 * Copyright (C) 2019 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://aws.amazon.com/freertos
 * http://www.FreeRTOS.org
 */

/**
 * @file aws_greengrass_discovery_demo.c
 * @brief A simple Greengrass discovery example.
 *
 * A simple example that perform discovery of the greengrass core device.
 * The JSON file is retrieved.
 */


/* Standard includes. */
#include <stdio.h>
#include <string.h>
#include <math.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "platform/iot_network.h"

/* Greengrass includes. */
#include "aws_ggd_config.h"
#include "aws_ggd_config_defaults.h"
#include "aws_greengrass_discovery.h"

/* MQTT includes. */
#include "aws_mqtt_agent.h"

/* Demo includes. */
#include "aws_demo_config.h"
#include "aws_clientcredential.h"

/* Includes for initialization. */
#include "iot_mqtt.h"

#define iotStatDEMO_MAX_MQTT_MESSAGES      3
#define iotStatDEMO_MAX_MQTT_MSG_SIZE      500
#define iotStatDEMO_DISCOVERY_FILE_SIZE    2500

#define iotStatDEMO_MQTT_MSG_TOPIC     "spoke/demos/iotstat/tStatRemote_01"
#define iotStatDEMO_MQTT_MSG \
	"{\"spoke__externalKey\": \"%s\","\
	 "\"spoke__mode\": \"device\","\
	 "\"spoke__messageTimestamp\": %i,"\
	 "\"spoke__thingType\": \"Environment\","\
	 "\"spoke__gatewayId\": \"Environment\","\
	 "\"temperature\": %0.3f,"\
	 "\"humidity\": %0.3f,"\
	 "\"lastSensorPoll\": %i"\
	"}"

#define iotStatDEMO_GG_RETRY_ATTEMPTS 5
#define iotStatDEMO_GG_RETRY_DELAY_SEC 5
#define iotStatDEMO_POLLING_PERIOD_SEC 6

#include "../../IoTStat/include/iotstat.h"
int sensorPin = GPIO_NUM_27;
float currentTemperature = 0.;
float currentHumidity = 0.;
int lastSensorSuccessEpoch = 0;

/* The maximum time to wait for an MQTT operation to complete.  Needs to be
 * long enough for the TLS negotiation to complete. */
static const TickType_t xMaxCommandTimeIot = pdMS_TO_TICKS( 20000UL );
static char pcJSONFileIoT[ iotStatDEMO_DISCOVERY_FILE_SIZE ];

/*
 * The MQTT client used for all the publish and subscribes.
 */
static MQTTAgentHandle_t xMQTTClientHandleIoT;
static BaseType_t connectToBroker( GGD_HostAddressData_t * pxHostAddressData );
bool findGreengrassCore( void * pvParameters );

static void publishMessage();
bool checkQuitPin();

GGD_HostAddressData_t xHostAddressData;

/*-----------------------------------------------------------*/

static BaseType_t connectToBroker( GGD_HostAddressData_t * pxHostAddressData )
{
    MQTTAgentConnectParams_t xConnectParams;
    BaseType_t xResult = pdPASS;

    /* Connect to the broker. */
    xConnectParams.pucClientId = ( const uint8_t * ) ( clientcredentialIOT_THING_NAME );
    xConnectParams.usClientIdLength = ( uint16_t ) ( strlen( clientcredentialIOT_THING_NAME ) );
    xConnectParams.pcURL = pxHostAddressData->pcHostAddress;
    xConnectParams.usPort = clientcredentialMQTT_BROKER_PORT;
    xConnectParams.xFlags = mqttagentREQUIRE_TLS | mqttagentURL_IS_IP_ADDRESS;
    xConnectParams.xURLIsIPAddress = pdTRUE; /* Deprecated. */
    xConnectParams.pcCertificate = pxHostAddressData->pcCertificate;
    xConnectParams.ulCertificateSize = pxHostAddressData->ulCertificateSize;
    xConnectParams.pvUserData = NULL;
    xConnectParams.pxCallback = NULL;
    xConnectParams.xSecuredConnection = pdTRUE; /* Deprecated. */

    if( MQTT_AGENT_Connect( xMQTTClientHandleIoT,
                            &xConnectParams,
                            xMaxCommandTimeIot ) != eMQTTAgentSuccess )
    {
        configPRINTF( ( "ERROR: Could not connect to the Broker.\r\n" ) );
        xResult = pdFAIL;
    }

    return xResult;
}

/*-----------------------------------------------------------*/

static void publishMessage()
{
	const char *pcTopic = iotStatDEMO_MQTT_MSG_TOPIC;
	char cBuffer[iotStatDEMO_MAX_MQTT_MSG_SIZE];
	( void )cBuffer;

	MQTTAgentPublishParams_t xPublishParams;
	MQTTAgentReturnCode_t xReturnCode;

	/* Connect */
	if (connectToBroker(&xHostAddressData) == pdPASS)
	{
		/* Publish */
		xPublishParams.xQoS = eMQTTQoS0;
		xPublishParams.pucTopic = (const uint8_t *)pcTopic;
		xPublishParams.usTopicLength = (uint16_t)(strlen(pcTopic));
		xPublishParams.ulDataLength = (uint32_t)sprintf(cBuffer, iotStatDEMO_MQTT_MSG,
														clientcredentialIOT_THING_NAME, // token 1: edge device thing name (max 128)
														xTaskGetTickCount(),						// token 2: time stamp in Epoch format (always 10)
														currentTemperature,				// token 3: sensed temperature (formated to 3 decimals, assume max 6 + decimal)
														currentHumidity,				// token 4: sensed humidity (formated to 3 decimals, assume max 6 + decimal)
														xTaskGetTickCount());		// token 5: the edge device clock value at the last successful poll (always 10)
		xPublishParams.pvData = cBuffer;
		xReturnCode = MQTT_AGENT_Publish(xMQTTClientHandleIoT,
										 &xPublishParams,
										 xMaxCommandTimeIot);

		if (xReturnCode != eMQTTAgentSuccess)
		{
			configPRINTF(("mqtt_client - Failure to publish \n"));
		}

		/* Disconnect */
		configPRINTF(("Disconnecting from broker.\r\n"));
		if (MQTT_AGENT_Disconnect(xMQTTClientHandleIoT,
								  xMaxCommandTimeIot) == eMQTTAgentSuccess)
		{
			configPRINTF(("Disconnected from the broker.\r\n"));
		}
		else
		{
			configPRINTF(("ERROR:  Did not disconnected from the broker.\r\n"));
		}
	}
}

/*-----------------------------------------------------------*/

bool checkQuitPin( )
{
    int tempSensorPollingQuitPin = GPIO_NUM_34;
    gpio_set_direction(tempSensorPollingQuitPin, GPIO_MODE_INPUT); // change to input mode

	return gpio_get_level(tempSensorPollingQuitPin) == 1;
}

/*-----------------------------------------------------------*/

bool findGreengrassCore(void * pvParameters)
{
	/* Unused Parameter */
	(void)pvParameters;

	bool greengrassConnected = false;

	/* Create MQTT Client. */
	if (MQTT_AGENT_Create( &(xMQTTClientHandleIoT) ) == eMQTTAgentSuccess)
	{
		/* Try up to iotStatDEMO_GG_RETRY_ATTEMPTS times to discover and connect to a local Greengrass Core */
		for (int inx = 0; inx < iotStatDEMO_GG_RETRY_ATTEMPTS; inx++)
		{
			memset(&xHostAddressData, 0, sizeof(xHostAddressData));

			configPRINTF( ("(%i) Attempting automated selection of Greengrass device ...\r\n", inx + 1) );

			if (GGD_GetGGCIPandCertificate(pcJSONFileIoT,
										   iotStatDEMO_DISCOVERY_FILE_SIZE,
										   &xHostAddressData)
				== pdPASS)
			{
				configPRINTF(("Greengrass device discovered!\r\n"));
				greengrassConnected = true;
				break;
			}
			else
			{
				configPRINTF( ("Auto-connect (%i): Failed to retrieve Greengrass address and certificate.\r\n", inx + 1) );
				greengrassConnected = false;
			}

			if (inx < iotStatDEMO_GG_RETRY_ATTEMPTS - 1)
			{
				configPRINTF(("... will attempt again in %i seconds.\r\n", iotStatDEMO_GG_RETRY_DELAY_SEC));
				ets_delay_us(iotStatDEMO_GG_RETRY_DELAY_SEC * 1000000);
			}
		}
	}
	return greengrassConnected;
}

/*-----------------------------------------------------------*/

int vGGtStatTask( bool awsIotMqttMode,
                                   const char * pIdentifier,
                                   void * pNetworkServerInfo,
                                   void * pNetworkCredentialInfo,
                                   const IotNetworkInterface_t * pNetworkInterface )
{
	/* Unused parameters */
	( void )awsIotMqttMode;
	( void )pIdentifier;
	( void )pNetworkServerInfo;
	( void )pNetworkCredentialInfo;
	( void )pNetworkInterface;

	uint32_t tic;
	if ( findGreengrassCore( NULL ) )
	{
		initTempSensor( sensorPin );
		tic = xTaskGetTickCount();
		configPRINTF( ("Starting polling loop at tick %i\r\n", tic) );
		while (1)
		{
			float elapsedTime_s = 0.;
			if (pollTempSensor(&currentTemperature, &currentHumidity))
			{
				configPRINTF( ("Temperature: %0.3f °C\r\n", currentTemperature) );
				configPRINTF( ("   Humidity: %0.3f %%\r\n", currentHumidity) );
				publishMessage();
			}
			
			configPRINTF( ("Waiting %is to poll... \r\n", iotStatDEMO_POLLING_PERIOD_SEC) );

			elapsedTime_s = ((xTaskGetTickCount() - tic) / portTICK_PERIOD_MS) / 10;
			while (elapsedTime_s < (iotStatDEMO_POLLING_PERIOD_SEC))
			{
				elapsedTime_s = ((xTaskGetTickCount() - tic) / portTICK_PERIOD_MS);
				if (fmod(elapsedTime_s, 1) == 0)
				{
					configPRINTF( ("... %0.2f ms\r\n", elapsedTime_s) );
				}
				ets_delay_us(10 * 1000);
			}
			configPRINTF( ("\r\n") );

			if (checkQuitPin())
			{
				break;
			}

			tic = xTaskGetTickCount();
		}
	}

	if (MQTT_AGENT_Delete(xMQTTClientHandleIoT) == eMQTTAgentSuccess)
	{
		configPRINTF(("Deleted Client.\r\n"));
	}
	else
	{
		configPRINTF(("ERROR:  MQTT_AGENT_Delete() Failed.\r\n"));
	}

	configPRINTF(("----Demo finished----\r\n"));

	vTaskDelete(NULL);

    return 0;
}

