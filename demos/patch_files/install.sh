#!/bin/bash
# installs the IoT Demo Patch files
mv ./demos/include/iot_demo_runner.h ./demos/include/iot_demo_runner.original.h
cp ./IoTStat/demos/patch_files/iot_demo_runner.h ./demos/include/iot_demo_runner.h

mv ./vendors/espressif/boards/esp32/aws_demos/config_files/aws_demo_config.h ./vendors/espressif/boards/esp32/aws_demos/config_files/aws_demo_config.original.h
cp ./IoTStat/demos/patch_files/aws_demo_config.h ./vendors/espressif/boards/esp32/aws_demos/config_files/aws_demo_config.h

mv ./CMakeLists.txt ./CMakeLists.original.txt
cp ./IoTStat/demos/patch_files/01_CMakeLists.txt ./CMakeLists.txt
