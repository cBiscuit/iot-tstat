# IoTStat (or IoT Thermostat)

Maintains the source and references for an overly complex IoT Thermostat.  
This project aims to provide an excuse to learn how to use the following:
1. AWS IoT Core
1. AWs Lambda
1. Amazon FreeRTOS
1. Amazon Greengrass

## Components

This project utilizes two main hardware components: Raspberry Pi (running AWS Greengrass) and an Espressif Devkit-C ESP32 microcontroller (running Amazon FreeRTOS).  The ESP32 is used as a second story thermostat, while the RPi is paired with a touchscreen and functions as the primary thermostat on the main level.

## Strategy

The remote thermostat (ESP32) publishes messages to the main thermostat (RPi) which aggregates the information through local Lambda functions, commands HVAC states, and sends status back to IoT core.  An application in IoT Core monitors information publlshed by the thermostat, aggregates this information with additional data from the cloud (weather data) and updates commanded states to the thermostat.

